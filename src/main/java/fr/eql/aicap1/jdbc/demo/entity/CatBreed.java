package fr.eql.aicap1.jdbc.demo.entity;

public enum CatBreed {

    BENGAL,
    NORVEGIEN,
    PERSAN,
    SIAMOIS,
    MUNCHKIN
}
