package fr.eql.aicap1.jdbc.demo.entity;

import java.util.ArrayList;
import java.util.List;

public class Owner {

    private Long id;
    private final String name;
    private final String surname;
    private final String login;
    private final String password;
    private List<Pet> pets = new ArrayList<>();
    private List<PetCategory> favoritePetCategories = new ArrayList<>();

    public Owner(Long id, String name, String surname, String login, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
    }

    /// Getters ///
    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public List<Pet> getPets() {
        return pets;
    }
    public List<PetCategory> getFavoritePetCategories() {
        return favoritePetCategories;
    }
}
