package fr.eql.aicap1.jdbc.demo.dao;

import fr.eql.aicap1.jdbc.demo.connection.PetPalDataSource;
import fr.eql.aicap1.jdbc.demo.entity.Cat;
import fr.eql.aicap1.jdbc.demo.entity.CatBreed;
import fr.eql.aicap1.jdbc.demo.entity.Owner;
import fr.eql.aicap1.jdbc.demo.entity.Pet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CatDao {

    private static final Logger logger = LogManager.getLogger();

    private static final String REQ_COUNT_BY_OWNER = "SELECT COUNT(*) AS count FROM pet WHERE pet.owner_id = ?";
    private static final String REQ_EXISTS =
            "SELECT * FROM cat " +
            "JOIN pet ON cat.pet_id = pet.pet_id " +
            "WHERE name = ?";
    private static final String REQ_FIND_BY_OWNER =
            "SELECT c.cat_id, p.name, cb.breed, p.birthdate, p.picture " +
            "FROM cat c " +
            "JOIN pet p ON c.pet_id = p.pet_id " +
            "JOIN cat_breed cb ON c.cat_breed_id = cb.id " +
            "WHERE p.owner_id = ? " +
            "ORDER BY p.name";
    private static final String REQ_INSERT_PET = "INSERT INTO pet (name, birthdate, picture, owner_id) VALUES (?,?,?,?)";
    private static final String REQ_INSERT_CAT = "INSERT INTO cat (pet_id, cat_breed_id) VALUES (?,?)";

    private DataSource dataSource = new PetPalDataSource();

    public int countByOwner(Owner owner) {
        int count = 0;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_COUNT_BY_OWNER);
            statement.setLong(1, owner.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation des animaux en base de données", e);
        }
        return count;
    }

    public boolean exists(Cat cat) {
        boolean exists = false;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_EXISTS);
            statement.setString(1, cat.getName());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                exists = true;
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation du chat en base de données", e);
        }
        return exists;
    }

    public List<Cat> findByOwner(Owner owner) {
        List<Cat> cats = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_FIND_BY_OWNER);
            statement.setLong(1, owner.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                cats.add(new Cat(
                        resultSet.getLong("cat_id"),
                        CatBreed.valueOf(resultSet.getString("breed")),
                        resultSet.getString("name"),
                        resultSet.getDate("birthdate").toLocalDate(),
                        resultSet.getString("picture")
                ));
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation des chats en base de données", e);
        }
        return cats;
    }

    public void insertCat(Cat cat, Owner owner) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            long id = petStatementExecution(cat, owner, connection);
            if (id > 0) {
                catStatementExecution(cat, connection);
            } else {
                connection.rollback();
            }
            connection.commit();
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de l'insertion de " + cat.getName() + " en base de données", e);
        }
    }

    private long petStatementExecution(Pet pet, Owner owner, Connection connection) throws SQLException {
        long id = 0;
        PreparedStatement statement = connection.prepareStatement(REQ_INSERT_PET, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, pet.getName());
        statement.setDate(2, Date.valueOf(pet.getBirthdate()));
        statement.setString(3, pet.getPicture());
        statement.setLong(4, owner.getId());
        int affectedRows = statement.executeUpdate();
        if (affectedRows > 0) {
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
                pet.setPetId(id);
            }
        }
        return id;
    }

    private void catStatementExecution(Cat cat, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(REQ_INSERT_CAT, Statement.RETURN_GENERATED_KEYS);
        statement.setLong(1, cat.getPetId());
        statement.setLong(2, cat.getBreed().ordinal() + 1);
        int affectedRow = statement.executeUpdate();
        if (affectedRow > 0) {
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                cat.setCatId(id);
            }
        } else {
            connection.rollback();
        }
    }
}
