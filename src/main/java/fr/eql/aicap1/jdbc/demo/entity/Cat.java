package fr.eql.aicap1.jdbc.demo.entity;

import java.time.LocalDate;

public class Cat extends Pet {

    private Long catId;
    private final CatBreed breed;

    public Cat(Long catId, CatBreed breed, String name, LocalDate birthdate, String picture) {
        super(name, birthdate, picture);
        this.catId = catId;
        this.breed = breed;
    }

    /// Getters ///
    public CatBreed getBreed() {
        return breed;
    }

    /// Setters ///
    public void setCatId(Long catId) {
        this.catId = catId;
    }
}
