package fr.eql.aicap1.jdbc.demo.program;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionTest {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        // Utilisateur de la bdd
        String username = "root";
        // Mot de passe de la bdd
        String password = "";
        // URL de la bdd avec certains paramètres pour MySQL 8
        String url = "jdbc:mysql://127.0.0.1:3306/petpal_db?serverTimezone=UTC&verifyServerCertificate=false&useSSL=true";
        // Connecteur MySQL pour JDBC
        String driver = "com.mysql.cj.jdbc.Driver";

        /* Connexion à la base de données, grâce à un 'try with resource' qui appellera la méthode 'close()'
        de l'objet 'connection' à la fin du 'try' afin de fermer la ressource. */
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            // On charge le connecteur (contenant les implémentations de MySQL pour JDBC) en mémoire.
            Class.forName(driver);
            /* Si la connexion s'est bien passée (URL, login et password corrects) et que le connecteur a été trouvé,
            le texte suivant sera imprimé en console. */
            System.out.println("Connexion OK");
        } catch (ClassNotFoundException e) {
            logger.error("Le connecteur MySQL est introuvable", e);
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la connexion avec la base de données.", e);
        }
    }
}
