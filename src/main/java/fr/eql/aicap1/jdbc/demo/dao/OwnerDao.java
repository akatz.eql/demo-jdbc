package fr.eql.aicap1.jdbc.demo.dao;

import fr.eql.aicap1.jdbc.demo.connection.PetPalDataSource;
import fr.eql.aicap1.jdbc.demo.entity.Owner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OwnerDao {

    private final static Logger logger = LogManager.getLogger();

    private static final String REQ_AUTH = "SELECT * FROM owner WHERE login = ? AND password = ?";
    private static final String REQ_EXISTS = "SELECT * FROM owner WHERE login = ?";
    private static final String REQ_INSERT_OWNER = "INSERT INTO owner (name, surname, login, password) VALUES (?,?,?,?)";
    private static final String REQ_FIND_ALL_BUT_SELF = "SELECT * FROM owner WHERE id != ? ORDER BY surname";

    private final DataSource dataSource = new PetPalDataSource();

    public Owner authenticate(String login, String password) {
        Owner owner = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_AUTH);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                owner = new Owner(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("login"),
                        resultSet.getString("password")
                );
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation du propriétaire en base de données", e);
        }
        return owner;
    }

    public boolean exists(Owner owner) {
        boolean exists = false;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_EXISTS);
            statement.setString(1, owner.getLogin());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                exists = true;
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation du propriétaire en base de données", e);
        }
        return exists;
    }

    public boolean insertOwner(Owner owner) {
        boolean isInserted = false;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_INSERT_OWNER);
            statement.setString(1, owner.getName());
            statement.setString(2, owner.getSurname());
            statement.setString(3, owner.getLogin());
            statement.setString(4, owner.getPassword());
            int returnedValue = statement.executeUpdate();
            isInserted = returnedValue > 0;
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de l'insertion du propriétaire en base de données", e);
        }
        return isInserted;
    }

    public List<Owner> findAllButSelf(Owner self) {
        List<Owner> owners = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_FIND_ALL_BUT_SELF);
            statement.setLong(1, self.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                owners.add(new Owner(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("login"),
                        resultSet.getString("password")
                ));
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation des propriétaires en base de données", e);
        }
        return owners;
    }
}
