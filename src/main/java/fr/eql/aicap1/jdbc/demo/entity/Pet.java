package fr.eql.aicap1.jdbc.demo.entity;

import java.time.LocalDate;

public class Pet {

    private Long petId;
    private final String name;
    private final LocalDate birthdate;
    private final String picture;

    public Pet(String name, LocalDate birthdate, String picture) {
        this.name = name;
        this.birthdate = birthdate;
        this.picture = picture;
    }

    /// Getters ///
    public Long getPetId() {
        return petId;
    }
    public String getName() {
        return name;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }
    public String getPicture() {
        return picture;
    }

    /// Setters ///
    public void setPetId(Long petId) {
        this.petId = petId;
    }
}
