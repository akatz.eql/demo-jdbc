package fr.eql.aicap1.jdbc.demo.entity;

public enum PetCategory {

    CHAT,
    CHIEN,
    LAPIN,
    HAMSTER,
    CHINCHILLA,
    OISEAU
}
