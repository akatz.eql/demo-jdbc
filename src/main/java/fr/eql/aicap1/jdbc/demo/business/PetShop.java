package fr.eql.aicap1.jdbc.demo.business;

import fr.eql.aicap1.jdbc.demo.dao.CatDao;
import fr.eql.aicap1.jdbc.demo.dao.OwnerDao;
import fr.eql.aicap1.jdbc.demo.entity.Cat;
import fr.eql.aicap1.jdbc.demo.entity.CatBreed;
import fr.eql.aicap1.jdbc.demo.entity.Owner;
import fr.eql.aicap1.jdbc.demo.entity.Pet;
import fr.eql.aicap1.jdbc.demo.util.DateUtils;
import fr.eql.aicap1.jdbc.demo.util.StringUtils;

import javax.swing.*;
import java.time.LocalDate;
import java.time.YearMonth;

public class PetShop {

    private static final String AUTHENTICATE = "1";
    private static final String REGISTER_OWNER = "2";
    private static final String DISPLAY_OWNER = "3";
    private static final String REGISTER_CAT = "4";
    private static final String DISPLAY_CATS = "5";
    private static final String QUIT = "6";

    private Owner owner;
    private String menuInput;

    private final OwnerDao ownerDao = new OwnerDao();
    private final CatDao catDao = new CatDao();

    public void enter() {
        do {
            showMenu();
            processMenuInput();
        } while (!menuInput.equals(QUIT));
        JOptionPane.showMessageDialog(null, "*** Au revoir et à bientôt chez Pet Pal !");
    }

    private void showMenu() {
        String message = "*** Pet Pal ***\r\n\r\n";
        message += AUTHENTICATE + " : S'authentifier\r\n";
        message += REGISTER_OWNER + " : S'enregistrer\r\n";
        if (owner != null) {
            message += DISPLAY_OWNER + " : Afficher mes informations\r\n";
            message += REGISTER_CAT + " : Enregistrer un chat\r\n";
        }
        if (owner != null && catDao.countByOwner(owner) > 0) {
            message += DISPLAY_CATS + " : Afficher mes chats\r\n";
        }
        message += QUIT + " : Quitter";
        menuInput = JOptionPane.showInputDialog(message);
    }

    private void processMenuInput() {
        switch (menuInput) {
            case AUTHENTICATE:
                authenticate();
                break;
            case REGISTER_OWNER:
                registerOwner();
                break;
            case DISPLAY_OWNER:
                displayOwner();
                break;
            case REGISTER_CAT:
                registerCat();
                break;
            case DISPLAY_CATS:
                displayCats();
                break;
            case QUIT:
                break;
            default:
                JOptionPane.showMessageDialog(null, "Mauvais choix");
        }
    }

    private void authenticate() {
        String login = JOptionPane.showInputDialog("Identifiant");
        String password = JOptionPane.showInputDialog("Mot de passe");
        owner = ownerDao.authenticate(login, password);
        if (owner == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Identifiant et/ou Mot de passe incorrect(s)."
            );
        } else {
            JOptionPane.showMessageDialog(
                    null,
                    "Bienvenue " + owner.getName() + " " + owner.getSurname()
            );
        }
    }

    private void registerOwner() {
        String name = JOptionPane.showInputDialog("Prénom");
        String surname = JOptionPane.showInputDialog("Nom");
        String login = JOptionPane.showInputDialog("Identifiant");
        String password = JOptionPane.showInputDialog("Mot de passe");
        Owner newOwner = new Owner(null, name, surname, login, password);
        boolean exists = ownerDao.exists(newOwner);
        if (exists) {
            JOptionPane.showMessageDialog(null,"Identifiant déjà attribué.");
        } else {
            boolean isRegistered = ownerDao.insertOwner(newOwner);
            if (isRegistered) {
                JOptionPane.showMessageDialog(null, name + " " + surname + " a bien été ajouté.");
            } else {
                JOptionPane.showMessageDialog(null, name + " " + surname + " n'a pas été ajouté.");
            }
        }
    }

    private void displayOwner() {
        JOptionPane.showMessageDialog(null,
                "*** Mes informations ***\r\n\r\n" +
                        "Prénom : " + owner.getName() + "\r\n" +
                        "Nom : " + owner.getSurname());
    }

    private void registerCat() {
        String name = JOptionPane.showInputDialog("Nom");
        String message = "Race :\r\n";
        for (int i = 0; i < CatBreed.values().length; i++) {
            message += (i + 1) + " : " + CatBreed.values()[i] + "\r\n";
        }
        CatBreed breed = CatBreed.values()[chooseNumber(message, CatBreed.values().length) - 1];
        int birthYear = chooseNumber("Année de naissance", LocalDate.now().getYear());
        int birthMonth = chooseNumber("Mois de naissance", 12);
        int lastDayOfMonth = YearMonth.of(birthYear, birthMonth).atEndOfMonth().getDayOfMonth();
        int birthDay = chooseNumber("Jour de naissance", lastDayOfMonth);
        LocalDate birthDate = LocalDate.of(birthYear, birthMonth, birthDay);
        Cat newCat = new Cat(null, breed, name, birthDate, "pictures/cat.jpg");
        boolean exists = catDao.exists(newCat);
        if (exists) {
            JOptionPane.showMessageDialog(null, "Un autre chat porte ce nom.");
        } else {
            catDao.insertCat(newCat, owner);
        }
    }


    private void displayCats() {
        owner.getPets().addAll(catDao.findByOwner(owner));
        String message = "*** Mes chats ***\r\n\r\n";
        int index = 1;
        for (Pet pet : owner.getPets()) {
            message += " - Chat " + index + " -\r\n";
            Cat cat = (Cat) pet;
            message += "Nom : " + cat.getName() + "\r\n";
            message += "Race : " + StringUtils.firstLetterCapitalized(cat.getBreed().toString()) + "\r\n";
            message += "Date de naissance : " + DateUtils.fullDate(cat.getBirthdate()) + "\r\n";
            message += "Image : " + cat.getPicture() + "\r\n";
            message += "***\r\n";
            index++;
        }
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * Demande en boucle à l'utilisateur de fournir un nombre,
     * tant que l'utilisateur fournit autre chose qu'un entier,
     * ou un entier dépassant la valeur limite supérieure indiquée.
     *
     * @param message Le message contextuel associé au nombre demandé.
     * @param upperLimit La limite supérieure autorisée pour le nombre.
     * @return Un entier entré par l'utilisateur.
     */
    private int chooseNumber(String message, int upperLimit) {
        boolean isValid = false;
        int number = 0;
        do {
            try {
                String input = JOptionPane.showInputDialog(message);
                number = Integer.parseInt(input);
                if (number <= upperLimit) {
                    isValid = true;
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Ce nombre dépasse la valeur autorisée.");
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null,
                        "Ceci n'est pas un nombre. Veuillez recommencer votre saisie.");
            }
        } while (!isValid);
        return number;
    }
}

