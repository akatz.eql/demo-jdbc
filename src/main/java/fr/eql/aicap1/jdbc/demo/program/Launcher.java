package fr.eql.aicap1.jdbc.demo.program;

import fr.eql.aicap1.jdbc.demo.business.PetShop;

public class Launcher {

    public static void main(String[] args) {

        PetShop petShop = new PetShop();
        petShop.enter();
    }
}
