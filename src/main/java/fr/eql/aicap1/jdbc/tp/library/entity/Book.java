package fr.eql.aicap1.jdbc.tp.library.entity;

import java.util.Objects;

public abstract class Book {

    private Long bookId;
    private Long isbn;
    private String title;
    private String author;
    private Category category;
    private Type type;

    public Book(Long isbn, String title, String author, Category category, Type type) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.category = category;
        this.type = type;
    }

    /// Getters ///
    public Long getBookId() {
        return bookId;
    }
    public Long getIsbn() {
        return isbn;
    }
    public String getTitle() {
        return title;
    }
    public String getAuthor() {
        return author;
    }
    public Category getCategory() {
        return category;
    }
    public Type getType() {
        return type;
    }

    /// Setters ///
    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return isbn.equals(book.isbn) && type == book.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, type);
    }
}
