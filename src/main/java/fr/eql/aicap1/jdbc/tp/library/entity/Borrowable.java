package fr.eql.aicap1.jdbc.tp.library.entity;

public class Borrowable extends Book {

    private Long borrowableId;
    private String borrower;
    private String returnDate;

    public Borrowable(Long borrowableId, Long isbn, String title, String author, Category category, Type type, String borrower, String returnDate) {
        super(isbn, title, author, category, type);
        this.borrowableId = borrowableId;
        this.borrower = borrower;
        this.returnDate = returnDate;
    }

    /// Getters ///
    public String getBorrower() {
        return borrower;
    }
    public String getReturnDate() {
        return returnDate;
    }

    /// Setters ///
    public void setBorrowableId(Long borrowableId) {
        this.borrowableId = borrowableId;
    }
}
