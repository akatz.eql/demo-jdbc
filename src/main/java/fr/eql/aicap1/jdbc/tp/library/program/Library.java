package fr.eql.aicap1.jdbc.tp.library.program;

import fr.eql.aicap1.jdbc.tp.library.dao.BookDao;
import fr.eql.aicap1.jdbc.tp.library.entity.*;

import javax.swing.JOptionPane;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author akatz
 *
 * Une bibliothèque virtuelle permettant d'enregistrer et afficher les informations de livres.
 * La bibliothèque est affichée par le biais d'une série de boites de dialogue prenant en entrée
 * les saisies clavier ou les clicks sur des boutons de l'utilisateur.
 */
public class Library {

    private static final int ADD_BOOKS = 1;
    private static final int SHOW_BOOKS = 2;
    private static final int EXIT = 3;

    /** Le Data Access Object permettant de manipuler des livres persistés. */
    private final BookDao bookDao = new BookDao();
    /** Les livres chargés dans la bibliothèque */
    private final Set<Book> books = new HashSet<>();
    /** Sert à stocker la saisie clavier de l'utilisateur dans les menus */
    private int menuInput;

    /** Entrée et opérations dans la bibliothèque, tant que l'utilisateur n'a pas choisi d'en sortir */
    public void enter() {
        bookDao.retrieveBooks(books);
        JOptionPane.showMessageDialog(null,"*** Bienvenue dans la bibliothèque ! ***");
        do {
            showMenuAndChoose();
            processMenuInput();
        } while (menuInput != EXIT);
        JOptionPane.showMessageDialog(null, "*** A bientôt dans la bibliothèque ! ***");
    }

    /** Affiche le menu et enregistre le choix entré par l'utilisateur */
    private void showMenuAndChoose() {
        String message = "Choisissez une option :" +
                "\r\n" + ADD_BOOKS + " : Enregistrer des livres.";
        if (!books.isEmpty()) {
            message += "\r\n" + SHOW_BOOKS + " : Afficher les livres.";
        }
        message += "\r\n" + EXIT + " : Sortir";

        menuInput = (int) chooseNumber(message, 1, 3);
    }

    /**
     * Demande en boucle à l'utilisateur de fournir un nombre,
     * tant que l'utilisateur fournit autre chose qu'un entier,
     * ou que ce nombre n'est pas compris entre les limites
     * spécifiées.
     *
     * Le retour de cette méthode peut être casté en 'int' si
     * les limites spécifiées le permettent.
     *
     * @param message Le message contextuel associé au nombre demandé.
     * @param lowerLimit La limite inférieure pour le nombre demandé.
     * @param upperLimit La limite supérieure pour le nombre demandé.
     * @return Un entier entré par l'utilisateur.
     */
    private long chooseNumber(String message, long lowerLimit, long upperLimit) {
        boolean isCorrectNumber = false;
        long number = 0;
        do {
            try {
                String input = JOptionPane.showInputDialog(message);
                number = Long.parseLong(input);
                if (number >= lowerLimit && number <= upperLimit) {
                    isCorrectNumber = true;
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Le nombre doit être compris entre " + lowerLimit + " et " + upperLimit + "." +
                                    "\r\nVeuillez recommencer votre saisie");
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null,
                        "Ceci n'est pas un nombre entier. Veuillez recommencer votre saisie.");
            }

        } while (!isCorrectNumber);
        return number;
    }

    /** Appelle le processus correspondant au choix effectué par l'utilisateur */
    private void processMenuInput() {
        switch (menuInput) {
            case ADD_BOOKS:
                saveBooks();
                break;
            case SHOW_BOOKS:
                displayBooks();
                break;
            default:
        }
    }

    /** Permet d'enregistrer des livres dans la bibliothèque */
    private void saveBooks() {
        int numberOfBooks = (int) chooseNumber(
                "Combien de livres souhaitez-vous enregistrer ?", 1, 100);
        for (int i = 0; i < numberOfBooks; i++) {
            JOptionPane.showMessageDialog(null, "Livre " + (i + 1));
            Type type = chooseType();
            long isbn = chooseNumber("ISBN", 1000000000L, 9999999999999L);
            String title = JOptionPane.showInputDialog("Titre");
            String author = JOptionPane.showInputDialog("Auteur");
            Category category = chooseCategory();
            Book book = null;
            switch (type) {
                case ACHETABLE:
                    int quantity = (int) chooseNumber("Quantité", 1, 1000);
                    float price = chooseNumber("Prix", 0, 1000);
                    book = new Buyable(null, isbn, title, author, category, type, quantity, price);
                    break;
                case EMPRUNTABLE:
                    book = new Borrowable(null, isbn, title, author, category, type, "", "");
                    break;
                default:
            }
            bookDao.createBook(book);
            bookDao.retrieveBooks(books);
        }
    }

    /** Permet de visualiser les informations des livres de la bibliothèque */
    private void displayBooks() {
        Category category = chooseCategory();
        Set<Book> selectedCategoryBooks = booksByCategory(category);
        String message = "Les livres de la catégorie : " + formatCategory(category) +"\r\n\r\n";
        int index = 0;
        for (Book b : selectedCategoryBooks) {
            index++;
            message += "Livre " + index + " :\r\n" +
                    "ISBN : " + b.getIsbn() +
                    "\r\nTitre : " + b.getTitle() +
                    "\r\nAuteur : " + b.getAuthor() +
                    "\r\nCatégorie : " + formatCategory(b.getCategory());
            switch (b.getType()) {
                case EMPRUNTABLE:
                    Borrowable borrowable = (Borrowable) b;
                    message += "\r\nEmprunteur : " + borrowable.getBorrower() +
                            "\r\nDate de retour : " + borrowable.getReturnDate();
                    break;
                case ACHETABLE:
                    Buyable buyable = (Buyable) b;
                    message += "\r\nQuantité : " + buyable.getQuantity() +
                            "\r\nPrix : " + buyable.getPrice();
                    break;
                default:
            }
            message += "\r\n\r\n******\r\n\r\n";
        }
        JOptionPane.showMessageDialog(null, message);
    }

    private Set<Book> booksByCategory(Category category) {
        Map<Category, Set<Book>> booksByCategory = new HashMap<>();
        Category[] categories = Category.values();
        for (Category c : categories) {
            booksByCategory.put(c, new HashSet<>());
        }
        for (Book b : books) {
            booksByCategory.get(b.getCategory()).add(b);
        }
        return booksByCategory.get(category);
    }

    /**
     * Permet à l'utilisateur de donner une catégorie de livres
     * parmi une liste proposée par le biais d'une boite de dialogue.
     *
     * @return La catégorie choisie par l'utilisateur.
     */
    private Category chooseCategory() {
        Category[] categories = Category.values();
        String message = "Catégorie de livre : \r\n";
        for (Category c : categories) {
            message += (c.ordinal() + 1) +
                    " : " +
                    formatCategory(c) +
                    "\r\n";
        }
        return categories[(int) chooseNumber(message, 1, categories.length) - 1];
    }

    private Type chooseType() {
        Type[] types = Type.values();
        String message = "Type de livre : \r\n";
        for (Type t : types) {
            message += (t.ordinal() + 1) +
                    " : " +
                    formatType(t) +
                    "\r\n";
        }
        return types[(int) chooseNumber(message, 1, types.length) - 1];
    }

    /**
     * Permet de formater une valeur d'énumération sous forme d'une chaîne de caractères
     * en minuscules, commençant par une majuscule, sans tiret.
     * @param category La catégorie sous forme d'une valeur d'énumération
     * @return Une chaîne de caractères formatée.
     */
    private String formatCategory(Category category) {
        String word = category.toString().toLowerCase().replace("_", " ");
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    private String formatType(Type type) {
        String word = type.toString().toLowerCase().replace("_", " ");
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}
