package fr.eql.aicap1.jdbc.tp.library.entity;

public class Buyable extends Book {

    Long buyableId;
    Integer quantity;
    Float price;

    public Buyable(Long buyableId, Long isbn, String title, String author, Category category, Type type, Integer quantity, Float price) {
        super(isbn, title, author, category, type);
        this.buyableId = buyableId;
        this.quantity = quantity;
        this.price = price;
    }

    /// Getters ///
    public Integer getQuantity() {
        return quantity;
    }
    public Float getPrice() {
        return price;
    }

    /// Setters ///
    public void setBuyableId(Long buyableId) {
        this.buyableId = buyableId;
    }
}
