package fr.eql.aicap1.jdbc.tp.library.entity;

public enum Category {

    LITTERATURE,
    HISTOIRE,
    SCIENCE_FICTION,
    ANTHROPOLOGIE
}
