package fr.eql.aicap1.jdbc.tp.library.entity;

public enum Type {

    EMPRUNTABLE,
    ACHETABLE
}
