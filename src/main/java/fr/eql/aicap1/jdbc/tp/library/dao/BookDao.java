package fr.eql.aicap1.jdbc.tp.library.dao;

import fr.eql.aicap1.jdbc.tp.library.connection.LibraryDataSource;
import fr.eql.aicap1.jdbc.tp.library.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Set;

public class BookDao {

    private static final Logger logger = LogManager.getLogger();

    private final DataSource dataSource = new LibraryDataSource();

    private static final String REQ_FIND_BORROWABLE =
            "SELECT bor.borrowable_id, bor.borrower, bor.return_date, b.isbn, b.title, b.author, c.category, t.type " +
                    "FROM borrowable bor " +
                    "JOIN book b ON b.book_id = bor.book_id " +
                    "JOIN category c ON c.id = b.category_id " +
                    "JOIN type t ON t.id = b.type_id";

    private static final String REQ_FIND_BUYABLE =
            "SELECT buy.buyable_id, buy.quantity, buy.price, b.isbn, b.title, b.author, c.category, t.type " +
                    "FROM buyable buy " +
                    "JOIN book b ON b.book_id = buy.book_id " +
                    "JOIN category c ON c.id = b.category_id " +
                    "JOIN type t ON t.id = b.type_id";

    private static final String REQ_INSERT_BOOK =
            "INSERT INTO book (isbn, title, author, category_id, type_id) VALUES (?,?,?,?,?)";

    private static final String REQ_INSERT_BORROWABLE =
            "INSERT INTO borrowable (borrower, return_date, book_id) VALUES (?,?,?)";

    private static final String REQ_INSERT_BUYABLE =
            "INSERT INTO buyable (quantity, price, book_id) VALUES (?,?,?)";

    /**
     * Permet de récupérer la liste des livres contenus
     * en base de données.
     * @param books La liste de livres dans laquelle les livres
     *              seront ajoutés
     */
    public void retrieveBooks(Set<Book> books) {
        logger.info("Récupération des infos livres sur le disque");
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(REQ_FIND_BORROWABLE);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                books.add(new Borrowable(
                        resultSet.getLong("borrowable_id"),
                        resultSet.getLong("isbn"),
                        resultSet.getString("title"),
                        resultSet.getString("author"),
                        Category.valueOf(resultSet.getString("category")),
                        Type.valueOf(resultSet.getString("type")),
                        resultSet.getString("borrower"),
                        resultSet.getString("return_date")
                ));
            }
            statement = connection.prepareStatement(REQ_FIND_BUYABLE);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                books.add(new Buyable(
                        resultSet.getLong("buyable_id"),
                        resultSet.getLong("isbn"),
                        resultSet.getString("title"),
                        resultSet.getString("author"),
                        Category.valueOf(resultSet.getString("category")),
                        Type.valueOf(resultSet.getString("type")),
                        resultSet.getInt("quantity"),
                        resultSet.getFloat("price")
                ));
            }
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de la consultation des livres en base de données.", e);
        }
    }

    /**
     * Permet d'enregistrer les informations d'un livre
     * en base de données.
     * @param book Le livre à enregistrer
     */
    public void createBook(Book book) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            long id = bookStatementExecution(book, connection);
            if (id > 0) {
                if (book.getType().equals(Type.EMPRUNTABLE)) {
                    borrowableStatementExecution(book, connection);
                } else {
                    buyableStatementExecution(book, connection);
                }
            } else {
                connection.rollback();
            }
            connection.commit();
        } catch (SQLException e) {
            logger.error("Une erreur s'est produite lors de l'insertion du livre '" + book.getTitle() + "' en base de données.", e);
        }
    }

    private long bookStatementExecution(Book book, Connection connection) throws SQLException {
        long id = 0;
        PreparedStatement statement = connection.prepareStatement(REQ_INSERT_BOOK, Statement.RETURN_GENERATED_KEYS);
        statement.setLong(1, book.getIsbn());
        statement.setString(2, book.getTitle());
        statement.setString(3, book.getAuthor());
        statement.setLong(4, book.getCategory().ordinal() + 1);
        statement.setLong(5, book.getType().ordinal() + 1);
        int affectedRows = statement.executeUpdate();
        if (affectedRows > 0) {
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                    book.setBookId(id);
                }
            } catch (SQLException e) {
                connection.rollback();
                logger.error("Une erreur s'est produite lors de la récupération de l'id du livre inséré.", e);
            }
        }
        return id;
    }

    private void borrowableStatementExecution(Book book, Connection connection) throws SQLException {
        Borrowable borrowable = (Borrowable) book;
        PreparedStatement statement = connection.prepareStatement(REQ_INSERT_BORROWABLE, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, borrowable.getBorrower());
        statement.setString(2, borrowable.getReturnDate());
        statement.setLong(3, borrowable.getBookId());
        int affectedRows = statement.executeUpdate();
        if (affectedRows > 0) {
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    long id = resultSet.getLong(1);
                    borrowable.setBorrowableId(id);
                }
            } catch (SQLException e) {
                connection.rollback();
                logger.error("Une erreur s'est produite lors de la récupération de l'id du livre empruntable inséré.", e);
            }
        }
    }

    private void buyableStatementExecution(Book book, Connection connection) throws SQLException {
        Buyable buyable = (Buyable) book;
        PreparedStatement statement = connection.prepareStatement(REQ_INSERT_BUYABLE, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, buyable.getQuantity());
        statement.setFloat(2, buyable.getPrice());
        statement.setLong(3, buyable.getBookId());
        int affectedRows = statement.executeUpdate();
        if (affectedRows > 0) {
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    long id = resultSet.getLong(1);
                    buyable.setBuyableId(id);
                }
            } catch (SQLException e) {
                connection.rollback();
                logger.error("Une erreur s'est produite lors de la récupération de l'id du livre achetable inséré.", e);
            }
        }
    }
}
