-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           10.4.24-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour library_db
DROP DATABASE IF EXISTS `library_db`;
CREATE DATABASE IF NOT EXISTS `library_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library_db`;

-- Listage de la structure de table library_db. book
DROP TABLE IF EXISTS `book`;
CREATE TABLE IF NOT EXISTS `book` (
  `book_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isbn` bigint(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  KEY `FK_book_type` (`type_id`),
  KEY `FK_book_category` (`category_id`),
  CONSTRAINT `FK_book_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_book_type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Listage des données de la table library_db.book : ~4 rows (environ)
INSERT INTO `book` (`book_id`, `isbn`, `title`, `author`, `type_id`, `category_id`) VALUES
	(1, 123456789123, 'Titre 1', 'Auteur 1', 1, 3),
	(2, 234567891234, 'Titre 2', 'Auteur 2', 2, 2),
	(3, 345678912345, 'Titre 3', 'Auteur 3', 1, 1),
	(4, 456789123456, 'Titre 4', 'Auteur 4', 2, 3);

-- Listage de la structure de table library_db. borrowable
DROP TABLE IF EXISTS `borrowable`;
CREATE TABLE IF NOT EXISTS `borrowable` (
  `borrowable_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `borrower` varchar(50) DEFAULT NULL,
  `return_date` varchar(50) DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`borrowable_id`),
  KEY `FK_borrowable_book` (`book_id`),
  CONSTRAINT `FK_borrowable_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table library_db.borrowable : ~2 rows (environ)
INSERT INTO `borrowable` (`borrowable_id`, `borrower`, `return_date`, `book_id`) VALUES
	(1, 'Mr Y', '25 mai', 1),
	(2, 'Mme X', '12 juin', 3);

-- Listage de la structure de table library_db. buyable
DROP TABLE IF EXISTS `buyable`;
CREATE TABLE IF NOT EXISTS `buyable` (
  `buyable_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`buyable_id`),
  KEY `FK_buyable_book` (`book_id`),
  CONSTRAINT `FK_buyable_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table library_db.buyable : ~2 rows (environ)
INSERT INTO `buyable` (`buyable_id`, `quantity`, `price`, `book_id`) VALUES
	(1, 10, 19, 2),
	(2, 12, 29, 4);

-- Listage de la structure de table library_db. category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Listage des données de la table library_db.category : ~4 rows (environ)
INSERT INTO `category` (`id`, `category`) VALUES
	(1, 'LITTERATURE'),
	(2, 'HISTOIRE'),
	(3, 'SCIENCE_FICTION'),
	(4, 'ANTHROPOLOGIE');

-- Listage de la structure de table library_db. type
DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Listage des données de la table library_db.type : ~2 rows (environ)
INSERT INTO `type` (`id`, `type`) VALUES
	(1, 'EMPRUNTABLE'),
	(2, 'ACHETABLE');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
